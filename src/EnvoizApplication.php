<?php

namespace Tiz\Envoiz;

use Exception;
use Symfony\Component\Console\Application;
use Symfony\Component\Yaml\Yaml;

class EnvoizApplication extends Application
{
    private $applicationDirectory;
    private $configuration;

    /**
     * @param string $name    The name of the application
     * @param string $version The version of the application
     */
    public function __construct(string $name = 'UNKNOWN', string $version = 'UNKNOWN', $applicationDirectory=null)
    {
        $this->applicationDirectory = $applicationDirectory;
        parent::__construct($name, $version);
    }

    public function getApplicationDirectory() {
        return $this->applicationDirectory;
    }

    public function getVendorDirectory() {
        return str_replace('/tiz/envoiz/bin', '', $this->applicationDirectory);
    }
    
    public function getRootDirectory() {
        return str_replace('/vendor/tiz/envoiz/bin', '', $this->applicationDirectory);
    }

}
