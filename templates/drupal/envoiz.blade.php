{{-- ===== --}}
{{-- HOOKS --}}
{{-- ===== --}}

{{-- By order of appearence --}}
{{-- Do not delete, keep them empty if unused --}}

{{-- init --}}
@task('hook_init')
    echo "=> Starting deployment..."
    cd {{ $config['app_path'] }}
    {{-- set maintenance mode --}}
    bin/drupal site:maintenance on
    {{-- dump database --}}
    bin/drupal database:dump --file={{ $config['root_path'] }}/{{ $dump_name }}
@endtask

{{-- git --}}
@task('hook_git_before')
    echo "==> Start git"
@endtask

@task('hook_git_after')
    echo "<= End git"
@endtask

{{-- composer --}}
@task('hook_composer_before')
    echo "=> Start composer"
@endtask

@task('hook_composer_after')
    echo "<= End composer"
@endtask

{{-- npm --}}
@task('hook_npm_before')
    echo "=> Start NPM"
@endtask

@task('hook_npm_after')
    echo "<= End NPM"
@endtask

{{-- complete --}}
@task('hook_complete')
    cd {{ $config['app_path'] }}
    {{-- unset maintenance mode --}}
    bin/drupal site:maintenance off
    {{-- reload caches --}}
    bin/drupal cr
    echo "<== Deployment completed successfully :)"
@endtask

