<?php

namespace Tiz\Envoiz\Console;

use Laravel\Envoy\TaskContainer;
use Laravel\Envoy\Compiler;
use Symfony\Component\Console\Input\InputOption;

class DeployCommand extends EnvoizCommand
{
    protected static $defaultName = 'deploy';

     /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this->ignoreValidationErrors(); 

        $this->setName('deploy')
            ->setDescription('Run the deployment process')
            ->addArgument('env', InputOption::VALUE_REQUIRED, 'The remote environment')
            ->addOption('no_git', null, InputOption::VALUE_NONE, 'Should the git task be fired ?')
            ->addOption('no_npm', null, InputOption::VALUE_NONE, 'Should the npm task be fired ?')
            ->addOption('no_composer', null, InputOption::VALUE_NONE, 'Should the composer task be fired ?')
            ->addOption('continue', null, InputOption::VALUE_NONE, 'Continue running even if a task fails')
            ->addOption('pretend', null, InputOption::VALUE_NONE, 'Dump Bash script for inspection')
            ->addOption('debug', null, InputOption::VALUE_NONE, 'Display debug infos')
            ;
    }

    /**
     * @inheritDoc
     */
    protected function additionnalOptions() {
        $parentOptions = parent::additionnalOptions();

        return array_merge($parentOptions, [
            'skip_npm' => $this->input->getOption('no_npm') ? true : false,
            'skip_composer' => $this->input->getOption('no_composer') ? true : false,
            'skip_git' => $this->input->getOption('no_git') ? true : false
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function loadTaskContainer() {
        $taskContainer = parent::loadTaskContainer();
        
        // Display summary
        $this->writeSummary();

        // Ask for confirmation
        $response = $this->ask("Is it okay? (Y/n)");
        
        // Yes by default
        if (!in_array($response, ["Y", "y", null])) {
            $this->output->writeln('Deployment aborted.');
            exit;
        }

        return $taskContainer;
    }

    /**
     * Write deployment summary in the output
     *
     * @return void
     */
    protected function writeSummary() {
        $this->output->writeln(
            'The <fg=yellow>' . $this->config['branch'] . '</> branch will be deployed on the ' .
            '<fg=yellow>' . $this->argument('env') . '</> environment'
        );

        $this->output->writeln('Use GIT:        <fg=yellow>' . ($this->option('no_git') == NULL ? 'Yes' : 'No') . '</>');

        $use_npm = ($this->config['use_npm'] == TRUE && $this->option('no_npm') == NULL);
        $this->output->writeln('Use NPM:        <fg=yellow>' . ($use_npm ? 'Yes' : 'No') . '</>');

        $use_composer = ($this->config['use_composer'] == TRUE && $this->option('no_composer') == NULL);
        $this->output->writeln('Use COMPOSER:   <fg=yellow>' . ($use_composer ? 'Yes' : 'No') . '</>');

        // display warning if current branch differs from config branch
        exec("git branch | grep \* | cut -d ' ' -f2", $return);
        $currentBranch = $return[0];
        if ($currentBranch != $this->config['branch']) {
            $this->output->writeln(
                '/!\ Warning: the local branch is <fg=yellow>' . $currentBranch . '</>' . 
                ', but <fg=yellow>' . $this->config['branch'] . '</> will be deployed'
            );
        }
    }
}