@servers(['web' => [$config['user'] . '@' . $config['host'] . ' -p ' . ($config['port'] ? $config['port'] : 22) ]])

@import('vendor/tiz/envoiz/envoy.tasks.blade.php')
@import('vendor/tiz/envoiz/envoy.stories.blade.php')

@import('envoiz.blade.php')