<?php

namespace Tiz\Envoiz\Console;

use Laravel\Envoy\Console\RunCommand;
use Laravel\Envoy\TaskContainer;
use Laravel\Envoy\Compiler;
use Symfony\Component\Console\Input\InputOption;
use Tiz\Envoiz\Configuration;

abstract class EnvoizCommand extends RunCommand
{

    protected $config;

    /**
     * Load the task container instance with the Envoy file.
     *
     * @return \Laravel\Envoy\TaskContainer
     */
    protected function loadTaskContainer()
    {
        // assert that environment is defined
        if (!$this->input->getArgument('env')) {
            throw new Exception('ERROR: environment is not defined in the command');
            exit;
        }

        // load configuration
        $config = new Configuration($this->getApplication()->getRootDirectory() . '/envoiz.config.yml');
        $this->config = $config->getByEnvironment($this->input->getArgument('env'));

        // display all options for debug purpose
        if ($this->input->getOption('debug')) {
            $this->writeDebug();
        }

        // define envoy file path
        $envoyFile = $this->getApplication()->getVendorDirectory() . '/tiz/envoiz/Envoy.blade.php';

        if (! file_exists($envoyFile)) {
            echo "{$envoyFile} not found.\n";

            exit(1);
        }

        with($container = new TaskContainer)->load(
            $envoyFile, new Compiler, array_merge(
                // CLI options
                $this->getOptions(),
                // contextual options 
                $this->additionnalOptions(),
                // config file content 
                ['config' => $this->config] 
            )
        );

        return $container;
    }


    protected function runTask($container, $task)
    {
        $macroOptions = $container->getMacroOptions($task);

        $confirm = $container->getTask($task, $macroOptions)->confirm;

        if ($confirm && ! $this->confirmTaskWithUser($task, $confirm)) {
            return;
        }

        if (($exitCode = $this->runTaskOverSSH($container->getTask($task, $macroOptions))) > 0) {
            foreach ($container->getErrorCallbacks() as $callback) {
                call_user_func($callback, $task);
            }

            return $exitCode;
        }

        foreach ($container->getAfterCallbacks() as $callback) {
            call_user_func($callback, $task);
        }
    }

    /**
     * Get the tasks from the container based on user input.
     *
     * @param  \Laravel\Envoy\TaskContainer  $container
     * @return array
     */
    protected function getTasks($container)
    {
        $tasks = [$task = $this->getName()];

        if ($macro = $container->getMacro($task)) {
            $tasks = $macro;
        }

        return $tasks;
    }

    /**
     * Set additional options to be assigned to the task runner
     * 
     * @return array
     */
    protected function additionnalOptions() {
        $datetime = new \DateTime('now');
        $datetime = $datetime->format('YmdHis');

        return [
            'env' => $this->input->getArgument('env'),
            'dump_name' => $this->getDumpName(),
            'datetime' => $datetime
        ];
    }
    
    /**
     * generate dump name 
     * format: {project_code}.{env}.{date}.sql
     */
    protected function getDumpName() {
        $datetime = new \DateTime('now');
        $datetime = $datetime->format('YmdHis');

        return strtolower($this->config['project_code'] . '.' . $this->input->getArgument('env') . '.' . $datetime . '.sql');
    }

    protected function writeDebug() {
        
        $allOptions = array_merge($this->input->getOptions(), $this->additionnalOptions());
        
        $options = '';
        
        $this->output->writeln("===OPTIONS===");
        foreach ($allOptions as $option => $value) {
            if ($value === false || $value === null ) {
                continue;
            }
            
            $this->output->writeln("--$option=$value");
        }
        $this->output->writeln("=============");
    }

}