@story('deploy', ['on' => 'web'])
    hook_init
    hook_git_before
    git
    hook_git_after
    hook_composer_before
    composer
    hook_composer_after
    hook_npm_before
    npm
    hook_npm_after
    hook_complete
@endstory