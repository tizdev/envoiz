<?php

namespace Tiz\Envoiz\Console;

use Symfony\Component\Console\Input\InputOption;

class InitCommand extends EnvoizCommand
{
    protected static $defaultName = 'init';

     /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this->ignoreValidationErrors(); 

        $this->setName('init')
            ->setDescription('Retrieve useful information about the current state of the remote')
            ->addArgument('env', InputOption::VALUE_REQUIRED, 'The remote environment')
            ->addOption('continue', null, InputOption::VALUE_NONE, 'Continue running even if a task fails')
            ->addOption('pretend', null, InputOption::VALUE_NONE, 'Dump Bash script for inspection')
            ->addOption('debug', null, InputOption::VALUE_NONE, 'Display debug infos')
            ;
    }

}