Envoiz v2.0.0

# Envoiz

Envoiz is a deployment tool made by Agence Tiz, using Laravel's Envoy https://laravel.com/docs/master/envoy

## Requirements

Current requirements are:
- having your local ssh key registered in the remote server
- having node and npm installed in your remote server
- be sure that your remote server has access to your git repository and has rights to `git clone` and `git pull`

## Setup local environment

Add envoiz as a dependency of your project:

- `cd /var/www` (or wherever is the .git is located)
- `composer init` (to create a composer.json file if it doesn't exist) 
- `composer config repositories.tiz composer http://packages.tiz.fr` (to enable tiz packages)
- `composer config secure-http false` (as http://packages.tiz.fr does not have https yet)
- `composer require --dev tiz/envoiz:^2.0.0` (add envoiz as a dev dependency)

Add composer binaries to your path

- `sudo ln -s /var/www/vendor/tiz/envoiz/bin/envoiz /usr/bin/envoiz`

(composer path may depend on your composer installation)

Init your local configuration files:

- `vendor/bin/envoiz_init <template>`

`<template>` is optionnal, currently available templates are:

- drupal (providing some useful commands for a drupal 8 application)
- blank (empty file with only a skeleton)

Rename the newly generated `envoiz.config.sample.yml` as `envoiz.config.yml` and make sure that paths and settings are correct. For example, you may also choose not to use composer or npm here.

If you are using NPM, make sure that your `package.json` file declares a `prod` script that will build your stuff.
eg:

```
{
  "name": "front_end",
  "scripts": {
    "prod": "npm run production",
    "production": "cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js"
  },
```

## Prepare remote (first use only)

To init the remote directory:

- `envoiz init <env>`

This will clone your project to the remote directory declared by the `root_path` in your `envoiz.config.yml`

## Deploy with Envoiz

Basic usage: 

`envoiz deploy <env>`

Options available:

- `<env>` the target environment, note that `<env>` must be declared in the configuration file

- `--no_git` skip the git part

- `--no_npm` skip the npm part

- `--no_composer` skip the composer part

The branch that will be deployed is the branch declared in your environment configuration by `branch` in your `envoiz.config.yml`

### Get status from remote:

`envoiz status <env>`

Displays some informations about the remote environment.

## Extend Envoiz

You can use hooks that are declared in `/envoiz.blade.php` to add custom commmands such as dumping the database, setting maintenance mode, ect...

While writing these hooks, you will have access to all variables defined in `envoiz.config.yml` inside a `$config` array (for instance: `root_path` will be `$config['root_path']`). 

You may even use your own custom variables by adding them to to your `envoiz.config.yml` or by specifying them as options of your CLI command. (eg. `envoiz deploy stage --foo="bar"`)

Those variables are also available:

- `$datetime` : the current datetime in the format "Ymd-His"

- `$branch` : the name of the branch that will be deployed

- `$env` : the name of the target environment

- `$dump_name` : generated name for a sql dump file, format is `{project_code}.{env}.{date}.sql`

- `$skip_git`, `$skip_composer`, `$skip_npm` if the corresponding flags were fired (--no_npm, --no_git, ect...)

You may take a look at `vendor/tiz/envoiz/envoy.tasks.blade.php` or `vendor/tiz/envoiz/templates/drupal/envoiz.blade.php` to have more hints about the way to write custom scripts for your hooks.