<?php

namespace Tiz\Envoiz;

use Exception;
use Symfony\Component\Yaml\Yaml;

class Configuration {

    private $rawConfig;

    public function __construct($file) {
        // check if config file exists
        if (!file_exists($file)) {
            throw new Exception('ERROR: config file "' . $file . '" does not exist!');
            exit;
        }

        // load configuration into an array
        $this->rawConfig = Yaml::parse(file_get_contents($file));
    }

    /**
     * Retrieve the environment specific configuration in an array
     *
     * @param string $env
     * @return array
     */
    public function getByEnvironment($env) {
        // merge "global" config with environment specific config
        if (!isset($this->rawConfig[$env])) {
            throw new Exception('ERROR: no settings for environment "' . $env . '"');
            exit;
        }

        $config = $this->rawConfig['all'];
        $config = array_merge($config, $this->rawConfig[$env]);
        
        if(!isset($config['php_path'])){
            $config['php_path'] = 'php';
        }

        return $config;
    }
}