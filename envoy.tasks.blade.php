{{-- =========== --}}
{{-- Basic tasks --}}
{{-- =========== --}}

@task('git')
    @if (!$skip_git)
        cd {{ $config['root_path'] }}

        echo "Git pull..."
        @if ($config['branch'])
            git pull origin {{ $config['branch'] }}
        @else
            git pull origin master
        @endif
    @else
        echo "git skipped"
    @endif
@endtask


@task('composer')
    @if ($config['use_composer'] && !$skip_composer)
        echo "Install vendors"
        cd {{ $config['app_path'] }}
        echo "{{$config['php_path']}} {{ $config['composer_path'] }} install"
        {{$config['php_path']}} {{ $config['composer_path'] }} install
    @else
        echo "composer skipped"
    @endif
@endtask

@task('npm')
    @if ($config['use_npm'] && !$skip_npm)
        cd {{ $config['npm_path'] }}

        echo "run npm"
        npm install
        
        echo "run build"
        npm run prod

        echo "remove node_modules"
        rm -rf node_modules
    @else
        echo "NPM skipped"
    @endif
@endtask

{{-- init git on remote directory --}}
@task('init')
    echo "=> Start init task"
    if [ ! -d {{ $config['root_path']}} ]; then
        echo "/!\ directory not found. Creating..."
        mkdir {{ $config['root_path']}}
    fi

    cd {{ $config['root_path'] }}

    echo "cloning from {{ $config['git_repository'] }} into {{ $config['root_path'] }}"
    git clone {{ $config['git_repository'] }} .

    echo "<= clone completed"
@endtask

{{-- ============= --}}
{{-- Utility tasks --}}
{{-- ============= --}}

@task('status', ['on' => 'web'])
    {{-- TODO: This as config --}}
    cd {{ $config['root_path'] }}
    
    echo 'Current branch';
    git branch

    echo 'Commit label';
    git log -1 --pretty=%B
    
    echo 'Hash:'
    git rev-parse HEAD
@endtask


{{-- In case of error we stop execution of tasks --}}
@error
    echo "$task failed";
    exit(1);
@enderror